<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Electivas extends Model
{
     //nombre de la tabla
	protected $table = 'electivas';
	//campos que permite
    protected $fillable = ['ELECTIVA','ID_PROFESOR','DESCRIPCION','CUPOS','CUPOS_DIS'];
	//ignoramos los campos create_at and update_at
    public $timestamps = false;
}
