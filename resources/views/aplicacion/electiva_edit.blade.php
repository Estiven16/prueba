@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
 @include('aplicacion.partial.mensajes')
	<h1>Editar Electiva</h1>
	<form  method="POST" action="{{ URL::to('/electiva_edit/'.$electiva->ID_ELECTIVA) }}">
	@include('aplicacion.partial.campos_electiva')
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</section>


@endsection