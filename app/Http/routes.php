<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication routes...
//Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('/', 'Auth\AuthController@getLogin');
/*Route::get('/', function () {
    return view('auth/login');
});*/

Route::group(['middleware' => 'auth'], function () {
	// administrador
	Route::get('/home','IndexController@index');
	Route::get('electivas','ElectivasController@index');
	Route::get('electiva_create','ElectivasController@create');
	Route::post('electiva_create','ElectivasController@store');
	Route::get('electiva_edit/{ID_ELECTIVA}', 'ElectivasController@edit');
    Route::post('electiva_edit/{ID_ELECTIVA}', 'ElectivasController@update');
    Route::get('electiva_delete/{ID_ELECTIVA}', 'ElectivasController@destroy');

	//estudiante
	Route::get('consultar_electivas','ElectivasController@show');
	Route::get('electiva_inscripcion/{ID_ELECTIVA}/{ID_ESTUDIANTE}','ElectivasController@inscripcion');
	Route::get('electiva_estudiantes/{ID_ELECTIVA}','ElectivasController@estudiantes');
	Route::get('electiva_profesor','ElectivasController@profesor');
	Route::post('electiva_profesor','ElectivasController@consulta_profesor');
   	//usuario
	Route::post('consulta','ElectivasController@consulta');

});