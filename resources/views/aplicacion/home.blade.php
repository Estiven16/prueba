@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
	@include('aplicacion.partial.mensajes')
	<h1>Bienvenido | {{ Auth::user()->nombre }}</h1>
	<br>
	<form method="POST" action="{{ url('consulta') }}">
	  {!! csrf_field() !!}
	  <div class="form-group">
	    <label for="">Ingrese el numero de cedula del Estudiante</label>
	    <input type="text" class="form-control" id="ID_ESTUDIANTE"  name="ID_ESTUDIANTE"  value="" placeholder="Cedula">
	  </div>
	  <button type="submit" class="btn btn-primary">Buscar</button>
	</form>
</section>
@endsection