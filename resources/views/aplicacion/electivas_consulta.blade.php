@extends('plantilla')

@section('contenido')

<section style="padding: 5% 0" class="container">
  @include('aplicacion.partial.mensajes')
	<h1>Electivas</h1>
	<a href="{{ url('electiva_profesor') }}" class="btn btn-primary" role="button">Ver electivas por profesor</a>
	<br><br>
	<table id="myTable" class="table table-striped table-bordered">
		<thead>
			<th>Nombre</th>
			<th>Profesor</th>
			<th>Descripcion</th>
			<th>Cupos</th>
			<th>Inscribirse</th>
			<th>Ver Estudiantes</th>
		</thead>
		<tbody>
			@foreach($electivas as $electiva)
			<tr>
				<td>{{ $electiva->ELECTIVA }}</td>
				<td>
				@foreach($profesores as $profesor)
					@if($electiva->ID_PROFESOR==$profesor->ID_PROFESOR)
						{{ $profesor->NOMBRE }}
					@endif
				@endforeach
				 </td>
				<td>{{ $electiva->DESCRIPCION }}</td>
				<td>{{ $electiva->CUPOS }}</td>
				<td><a href="{{ url('electiva_inscripcion'.'/'.$electiva->ID_ELECTIVA.'/'.Auth::user()->identificacion) }}">Inscribirse</a></td>
				<td><a href="{{ url('electiva_estudiantes', $electiva->ID_ELECTIVA) }}">Ver</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</section>
<script type="text/javascript">
	$(function(){
    $('#myTable').DataTable();
	});
</script>
@endsection