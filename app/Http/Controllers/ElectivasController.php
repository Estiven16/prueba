<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Electivas;
use App\Profesores;
use Auth;
use DB;

class ElectivasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	if(Auth::user()->rol=='3'){
    		$electivas = Electivas::all();
    		$profesores = Profesores::all();
        	return view('aplicacion.electivas')->with('electivas',$electivas)->with('profesores',$profesores);
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if(Auth::user()->rol=='3'){
    		$profesores = Profesores::all();
	        return view('aplicacion.electiva_create')->with('profesores',$profesores);
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
        'ELECTIVA'=>'required',
        'ID_PROFESOR'=>'required',
        'CUPOS'=>'required',
        'DESCRIPCION'=>'required',
        ]);

	    Electivas::create($request->all());

	    $request->session()->flash('alert-success', 'La electiva fue creada');
           
        return Redirect::to('electivas')->send();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if(Auth::user()->rol=='2'){
    		$electivas = Electivas::all();
    		$profesores = Profesores::all();
        	return view('aplicacion.electivas_consulta')->with('electivas',$electivas)->with('profesores',$profesores);
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
    }

    public function inscripcion(Request $request,$id_electiva,$id_estudiante){
    	if(Auth::user()->rol=='2'){
    		
    		$electivas = DB::table('electivas_estudiante')
            ->where('ID_ESTUDIANTE',$id_estudiante)
            ->where('ID_ELECTIVA',$id_electiva)
            ->get();


            if (empty($electivas)) {
            	DB::table('electivas_estudiante')->insert(
				    ['ID_ELECTIVA' => $id_electiva, 'ID_ESTUDIANTE' => $id_estudiante]
				);
				$request->session()->flash('alert-success', 'Registro exitoso');
           
        		return Redirect::to('consultar_electivas')->send();

            }else{
            	$request->session()->flash('alert-danger', 'Usted ya esta registrado en esta electiva');
    			return Redirect::to('consultar_electivas')->send();
            }

    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
    }

    public function estudiantes(Request $request,$id_electiva){

    	if(Auth::user()->rol=='2'){
    		
    		$electiva = DB::table('electivas')
            ->where('ID_ELECTIVA',$id_electiva)
            ->first();


	        $profesor = DB::table('profesor')
	            ->where('ID_PROFESOR',$electiva->ID_PROFESOR)
	            ->first();

	        $estudiantes = DB::table('electivas_estudiante')
	            ->where('ID_ELECTIVA',$id_electiva)
	            ->get();

	        $usuarios = User::all(); 

	        return view('aplicacion.electiva_estudiante')->with('profesor',$profesor)->with('electiva',$electiva)->with('estudiantes',$estudiantes)->with('usuarios',$usuarios);

    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}

    	
    }

    public function consulta(Request $request){

        if(Auth::user()->rol=='1'){
            
            $electivas = Electivas::all();


            $profesores = Profesores::all();

            $estudiante_electivas = DB::table('electivas_estudiante')
                ->where('ID_ESTUDIANTE',$request->only('ID_ESTUDIANTE'))
                ->get();

            $usuario = DB::table('users')
            ->where('identificacion',$request->only('ID_ESTUDIANTE'))
            ->first();

            return view('aplicacion.estudiante_electivas')->with('profesores',$profesores)->with('electivas',$electivas)->with('estudiante_electivas',$estudiante_electivas)->with('usuario',$usuario);

        }else{
            $request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
            return Redirect::to('home')->send();
        }

        
    }

    public function profesor(){

    	if(Auth::user()->rol=='2'){
    		$profesores = Profesores::all();
        	return view('aplicacion.electivas_profesores')->with('profesores',$profesores);
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
    	
    }

    public function consulta_profesor(Request $request){

    	$electivas = DB::table('electivas')
            ->where('ID_PROFESOR',$request->only('ID_PROFESOR'))
            ->get();

        $profesores = Profesores::all();

        return view('aplicacion.electivas_consulta')->with('electivas',$electivas)->with('profesores',$profesores);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol=='3'){
        	$electiva = DB::table('electivas')
            ->where('ID_ELECTIVA',$id)
            ->first();
    		$profesores = Profesores::all();
	        return view('aplicacion.electiva_edit')->with('profesores',$profesores)->with('electiva',$electiva);
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
        'ELECTIVA'=>'required',
        'ID_PROFESOR'=>'required',
        'CUPOS'=>'required',
        'DESCRIPCION'=>'required',
        ]);

        DB::table('electivas')
            ->where('ID_ELECTIVA',$id)
            ->update($request->only('ELECTIVA','ID_PROFESOR','CUPOS','DESCRIPCION'));

        $request->session()->flash('alert-success', 'La electiva fue actualizada');
        return Redirect::to('electivas')->send();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id )
    {
        

		if(Auth::user()->rol=='3'){
        	$deleted = DB::table('electivas')->where('ID_ELECTIVA', '=', $id)->delete();
			$request->session()->flash('alert-success', 'La Electiva Fue Eliminada');
			return  Redirect::to('electivas'); 
    	}else{
    		$request->session()->flash('alert-danger', 'Usted no tiene permiso para acceder a este modulo');
    		return Redirect::to('home')->send();
    	}     
	}
}
