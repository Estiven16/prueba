<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesores extends Model
{

 //nombre de la tabla
	protected $table = 'profesor';
	//campos que permite
    protected $fillable = ['ID_PROFESOR','NOMBRE'];
	//ignoramos los campos create_at and update_at
    public $timestamps = false;


}
