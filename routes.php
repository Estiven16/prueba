<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/home', function () {
    return view('aplicacion/home');
});


/*Route::get('/login', function () {
    return view('login');
});*/
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('/', function () {
    return redirect()->to('auth/login');
});
//

Route::group(['middleware' => 'auth'], function () {
	// Registration routes...
	Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');
	//index
    Route::get('home', 'DashboardController@index');
    //crear ciudades
    Route::get('ciudad_create', 'CiudadController@create');
    Route::post('ciudad_create', 'CiudadController@store');
    //ciudades
    Route::get('ciudades', 'CiudadController@index');
    //editar ciudades
    Route::get('ciudad_edit/{COD_CIUDAD}', 'CiudadController@edit');
    Route::post('ciudad_edit/{COD_CIUDAD}', 'CiudadController@update');
    //Afiliado
    Route::get('asociado','AfiliadoController@show');
    Route::post('asociado','AfiliadoController@show');
    //asociados
    Route::get('asociados','AfiliadoController@index');
    //crear asociados
    Route::post('asociado_create','AfiliadoController@store');
    Route::get('asociado_create','AfiliadoController@create');
    //editar asociados
    Route::get('asociado_edit/{id}', 'AfiliadoController@edit');
    Route::post('asociado_edit/{id}', 'AfiliadoController@update');
    //eliminar asociados
    Route::get('asociado_delete/{id}', 'AfiliadoController@destroy');
    //Boletas
    Route::get('boleta/{id}/{cod_ciudad?}/{id_producto}/{id_planeacion?}','BoletaController@create');
    Route::post('boleta','BoletaController@store');
    Route::get('boletas','BoletaController@index');
    //editar boletas
    Route::get('boleta_edit/{id}','BoletaController@edit');
    Route::post('boleta_edit/{id}','BoletaController@update');
    //eliminar boletas
    Route::get('boleta_delete/{id}','BoletaController@destroy');
    //Empleados
    Route::get('empleados','EmpleadoController@index');
    //crear empleados
    Route::get('empleado_create','EmpleadoController@create');
    Route::post('empleado_create','EmpleadoController@store');
    //editar empleados
    Route::get('empleado_edit/{id}','EmpleadoController@edit');
    Route::post('empleado_edit/{id}','EmpleadoController@update');
    //productos
    Route::get('productos','ProductoController@index');
    //crear productos
    Route::get('producto_create','ProductoController@create');
    Route::post('producto_create','ProductoController@store');
    //editar productos
    Route::get('producto_edit/{id}','ProductoController@edit');
    Route::post('producto_edit/{id}','ProductoController@update');
    //planeaciones
    Route::get('planeaciones','PlaneacionController@index');
    //crear planeacion
    Route::get('planeacion_create','PlaneacionController@create');
    Route::post('planeacion_create','PlaneacionController@store');
    //editar planeacion
    Route::get('planeacion_edit/{id}', 'PlaneacionController@edit');
    Route::post('planeacion_edit/{id}', 'PlaneacionController@update');
    //eliminar planeacion
    Route::delete('planeacion_delete/{id}', 'PlaneacionController@destroy');
    //PDF
    Route::get('pdf/{id}', 'PdfController@invoice');
    Route::get('generar_informe', 'PdfController@create');
    Route::post('generar_informe', 'PdfController@informe');
    //asociados excel
    Route::get('asociado_export', 'PdfController@exportar_asociados');
    //planeacion excel
    Route::get('planeacion_export', 'PdfController@exportar_planeacion');
     //boletas excel
    Route::get('boletas_export', 'PdfController@exportar_boletas');
    // Podemos usar este grupo para incluir todas las páginas que vayan a usar este middleware
    //Route::get('dashboard2', 'DashboardController@index2');
    //Route::get('dashboard3', 'DashboardController@index3');
});

