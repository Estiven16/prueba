@extends('welcome')

@section('contenido')
@if($errors->has())
            <div class="alert alert-danger" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>   
<form  method="POST" action="{{ URL::to('/auth/register') }}">
	 {!! csrf_field() !!}
  <div class="form-group">
    <label for="exampleInputName">Nombre</label>
    <input type="text" class="form-control" id="exampleInputName"  name="nombre"  value="{{ old('name') }}" placeholder="Name">
  </div>
  <div class="form-group">
    <label for="exampleInputId">Identificacion</label>
    <input type="text" class="form-control" id="exampleInputId"  name="identificacion"  value="{{ old('identificacion') }}" placeholder="Identificacion">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail1"  name="email"  value="{{ old('email') }}" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputRol">Rol</label>
    <select  class="form-control" id="exampleInputRol" name='rol'>
    	<option value='1'>Usuario</option>
    	<option value='2'>Estudiante</option>
    	<option value='3'>Administrador</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword2">Password confirmation</label>
    <input type="password" class="form-control" name="password_confirmation" id="password-confirmation" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Ingresar</button>
</form>

@endsection
