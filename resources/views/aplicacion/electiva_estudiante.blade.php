@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
 @include('aplicacion.partial.mensajes')
	<h1>Estudiantes Registrado a Electiva</h1>
	
	 <table class="table table-striped table-bordered">
      <tr>
        <td><b>Nombre</b></td><td>{{$electiva->ELECTIVA}}</td><td><b>Profesor</b></td><td>{{$profesor->NOMBRE}}</td>
      </tr> 
      <tr>
        <td><b>Cupos</b></td><td>{{$electiva->CUPOS}}</td><td><b>Descripcion</b></td><td>{{$electiva->DESCRIPCION}}</td>
      </tr>
   </table>
	<div class="form-group">
      <table class="table table-striped table-bordered">
      <tr>
        <th>Identificacion</th>
        <th>Nombre</th>
      </tr>
      @foreach($estudiantes as $estudiante)
        @foreach($usuarios as $usuario)
          @if($estudiante->ID_ESTUDIANTE == @$usuario->identificacion)
            <tr>
              <td>{{@$usuario->identificacion}}</td><td>{{@$usuario->nombre}}</td>
            </tr>
          @endif
      @endforeach
    @endforeach
    </table>
  </div>

  <a href="{{ url('consultar_electivas') }}" class="btn btn-primary">Atras</a>
</section>


@endsection