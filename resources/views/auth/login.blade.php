

<!-- resources/views/auth/login.blade.php -->
@extends('welcome')

@section('contenido')
@if($errors->has())
            <div class="alert alert-danger" role="alert">
               @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
              @endforeach
            </div>
        @endif </br>   
<form  method="POST" action="{{ URL::to('/auth/login') }}">
	 {!! csrf_field() !!}
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail1"  name="email"  value="{{ old('email') }}" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Ingresar</button>
  <a href="{{ url('auth/register') }}" class="btn btn-primary">Registrarse</a>
</form>

@endsection

