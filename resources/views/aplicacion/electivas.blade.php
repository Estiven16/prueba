@extends('plantilla')

@section('contenido')

<section style="padding: 5% 0" class="container">
  @include('aplicacion.partial.mensajes')
	<h1>Electivas</h1>
	<a href="{{ url('electiva_create') }}" class="btn btn-primary" role="button">Crear Electiva</a>
	<br><br>
	<table id="myTable" class="table table-striped table-bordered">
		<thead>
			<th>Nombre</th>
			<th>Profesor</th>
			<th>Descripcion</th>
			<th>Cupos</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			@foreach($electivas as $electiva)
			<tr>
				<td>{{ $electiva->ELECTIVA }}</td>
				<td>
				@foreach($profesores as $profesor)
					@if($electiva->ID_PROFESOR==$profesor->ID_PROFESOR)
						{{ $profesor->NOMBRE }}
					@endif
				@endforeach
				 </td>
				<td>{{ $electiva->DESCRIPCION }}</td>
				<td>{{ $electiva->CUPOS }}</td>
				<td><a href="{{ url('electiva_edit', $electiva->ID_ELECTIVA) }}">Editar</a></td>
				<td><a href="{{ url('electiva_delete', $electiva->ID_ELECTIVA) }}">Eliminar</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</section>
<script type="text/javascript">
	$(function(){
    $('#myTable').DataTable();
	});
</script>
@endsection