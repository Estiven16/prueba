 {!! csrf_field() !!}
  <div class="form-group">
    <label for="name">Nombre Electiva</label>
    <input type="text" class="form-control" id="ELECTIVA"  name="ELECTIVA"  value="@if (@$electiva->ELECTIVA!=NULL) {{ $electiva->ELECTIVA }} @endif" placeholder="Nombre Electiva">
  </div>
  <div class="form-group">
    <label for="tex_agencia">Profesor</label>
    <select class="form-control" name="ID_PROFESOR">
      <option value=''>Seleccione una Opcion</option>
      @foreach($profesores as $profesor)
          @if($profesor->ID_PROFESOR == @$electiva->ID_PROFESOR)
            <option value="{{$profesor->ID_PROFESOR}}" selected="selected">{{ $profesor->NOMBRE }}</option>
          @else
              <option value="{{$profesor->ID_PROFESOR}}">{{ $profesor->NOMBRE }}</option>
          @endif
      @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="name">Cupos</label>
    <input type="text" class="form-control" id="CUPOS"  name="CUPOS"  value="@if (@$electiva->CUPOS!=NULL) {{ $electiva->CUPOS }} @endif" placeholder="Cupos">
  </div>
  <div class="form-group">
    <label for="name">Descripcion</label>
    <input type="text" class="form-control" id="DESCRIPCION"  name="DESCRIPCION"  value="@if (@$electiva->DESCRIPCION!=NULL) {{ $electiva->DESCRIPCION  }}@endif" placeholder="Descripcion">
  </div>
  