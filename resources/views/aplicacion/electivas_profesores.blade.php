@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
 @include('aplicacion.partial.mensajes')
	<h1>Consultar Electivas por Profesor</h1>
	<form  method="POST" action="{{ URL::to('/electiva_profesor') }}">
	 {!! csrf_field() !!}
	<div class="form-group">
    <label for="tex_agencia">Profesor</label>
    <select class="form-control" name="ID_PROFESOR">
      <option value=''>Seleccione una Opcion</option>
      @foreach($profesores as $profesor)
          @if($profesor->ID_PROFESOR == @$electiva->ID_PROFESOR)
            <option value="{{$profesor->ID_PROFESOR}}" selected="selected">{{ $profesor->NOMBRE }}</option>
          @else
              <option value="{{$profesor->ID_PROFESOR}}">{{ $profesor->NOMBRE }}</option>
          @endif
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Consultar</button>
</form>
</section>


@endsection