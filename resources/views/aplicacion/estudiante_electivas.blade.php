@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
 @include('aplicacion.partial.mensajes')
	<h1>Estudiantes Registrado a Electiva</h1>
	
	 <table class="table table-striped table-bordered">
      <tr>
        <td><b>Nombre</b></td><td>{{$usuario->nombre}}</td><td><b>Identificacion</b></td><td>{{$usuario->identificacion}}</td>
      </tr> 
   </table>
	<div class="form-group">
      <table class="table table-striped table-bordered">
      <tr>
        <th>Electiva</th>
        <th>Profesor</th>
      </tr>
      @foreach($estudiante_electivas as $row)
        @foreach($electivas as $electiva)
          @if($row->ID_ELECTIVA == @$electiva->ID_ELECTIVA)
            <tr>
              <td>{{$electiva->ELECTIVA}}</td>
              <td>
                  @foreach($profesores as $profesor)
                    @if($electiva->ID_PROFESOR==$profesor->ID_PROFESOR)
                      {{ $profesor->NOMBRE }}
                    @endif
                  @endforeach
              </td>
            </tr>
          @endif
      @endforeach
    @endforeach
    </table>
  </div>

  <a href="{{ url('home') }}" class="btn btn-primary">Atras</a>
</section>


@endsection