@extends('plantilla')

@section('contenido')

<section style="padding: 10%" class="container">
 @include('aplicacion.partial.mensajes')
	<h1>Crear Electiva</h1>
	<form  method="POST" action="{{ URL::to('/electiva_create') }}">
	@include('aplicacion.partial.campos_electiva')
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</section>


@endsection